
;; SXML playground

(define (attr-node? n)
  (and (pair? n) (eqv? '@ (car n))))


;; TODO: Also skip special elements such as *COMMENT*?
(define (text loc)
  (call-with-output-string
    (lambda (out)
      (let loop ((loc (zip-scope loc)))
        (when (and loc (not (zip-end? loc)))
          (let ((n (zip-node loc)))
            (cond ((string? n)
                   (display n out)
                   (loop (zip-next loc)))
                  ((attr-node? n)
                   (loop (zip-right loc)))
                  (else
                   (loop (zip-next loc))))))))))

(define ((has-class class) loc)
  (and-let* ((attrs (zip-find-child loc (tag= '@)))
             (classes (zip-find-child attrs (tag= 'class))))
    (irregex-search `(seq bow ,class eow) (text classes))))

(define ((equals x) loc)
  (equal? x (zip-node loc)))

(define ((tag= x) loc)
  (and loc
       (let ((n (zip-node loc)))
         (and (pair? n) (eqv? (car n) x)))))

(define (sxml-element? n)
  (and (pair? n) (symbol? (car n))))

(define (make-sxml-node node children)
  (cons (car node) children))

(define sxml-zipper
  (zipper sxml-element? cdr make-sxml-node 'sxml-zipper))

(define p
  (sxml-zipper
   '(person
     (first-name "foo")
     (last-name "bar"))))


;; (-> (seq-zip '(1 (2 (3 4))))
;;     down right down right down node) => 3



;; (define page
;;   (call-with-input-file "qwantz.html" html->sxml))

;; '(// (div (@ (equal? (class "randomquote")))) a *text*)

(use html-parser clojurian-syntax http-client)

(define (random-qwantz-quote)
  (and-let* ((x (call-with-input-request "http://www.qwantz.com/index.php" #f html->sxml))
             (x (sxml-zipper x))
             (x (zip-find x (equals "randomquote")))
             (x (zip-find-parent x (tag= 'div)))
             (x (zip-filter x (tag= 'a))))
    (text (last x))))

