(cond-expand
  (dev
   (load-relative "../zipper.scm"))
  (else
   (require-library zipper)))

(import zipper)
(use test)

(test-begin)

(test-group "zip-node, zip-children, zip-branch?"
  (define z (list-zipper '(a b c)))
  (test '(a b c) (zip-node z))
  (test '(a b c) (zip-children z))
  (test-assert (zip-branch? z))
  (test-assert (not (zip-branch? (list-zipper 'x)))))

(test-group "zip-up, zip-down, zip-left, zip-right, zip-root"
  (define z0 (list-zipper '(a (b c))))
  (test '(a (b c)) (zip-node z0))
  
  (define z1 (zip-down z0))
  (test 'a (zip-node z1))

  (define z2 (zip-right z1))
  (test '(b c) (zip-node z2))

  (define z3 (zip-down z2))
  (test 'b (zip-node z3))

  (define z4 (zip-right z3))
  (test 'c (zip-node z4))

  (define z5 (zip-left z4))
  (test (zip-node z3) (zip-node z5))

  (define z6 (zip-up z5))
  (test (zip-node z2) (zip-node z6))

  (define z7 (zip-up z6))
  (test (zip-node z0) (zip-node z7))
  (test (zip-node z0) (zip-node (zip-root z4))))

(test-group "zip-rightmost, zip-leftmost"
  (define z0 (list-zipper '(a b c d)))
  (test (zip-node z0) (zip-node (zip-leftmost z0)))
  (test (zip-node z0) (zip-node (zip-rightmost z0)))

  (define z1 (zip-down z0))
  (define z2 (zip-rightmost z1))
  (test 'd (zip-node z2))
  (test 'a (zip-node (zip-leftmost z2))))


(test-group "zip-prepend-child, zip-append-child, zip-replace, zip-edit"
  (define z0 (list-zipper '(a (b (c)))))

  (define z1 (zip-right (zip-down (zip-right (zip-down z0)))))
  
  (define z2 (zip-prepend-child z1 'd))
  (test '(d c) (zip-node z2))
  (test '(a (b (d c))) (zip-node (zip-root z2)))
  (test '(a (b (c))) (zip-node z0))

  (define z3 (zip-append-child z1 'd))
  (test '(c d) (zip-node z3))
  (test '(a (b (c d))) (zip-node (zip-root z3)))
  (test '(a (b (c))) (zip-node z0))

  (define z4 (zip-replace z1 'd))
  (test 'd (zip-node z4))
  (test '(a (b d)) (zip-node (zip-root z4)))
  (test '(a (b (c))) (zip-node z0))

  (define z5 (zip-edit z1 append '(d)))
  (test '(c d) (zip-node z5))
  (test '(a (b (c d))) (zip-node (zip-root z5)))
  (test '(a (b (c))) (zip-node z0)))

(test-group "zip-insert-left, zip-insert-right"
  (define z0 (zip-down (list-zipper '(c))))
  
  (define z1 (zip-insert-left z0 'a))
  (test '(a c) (zip-node (zip-root z1)))
  
  (define z2 (zip-insert-left z1 'b))
  (test '(a b c) (zip-node (zip-root z2)))

  (define z3 (zip-insert-right z2 'e))
  (test '(a b c e) (zip-node (zip-root z3)))

  (define z4 (zip-insert-right z3 'd))
  (test '(a b c d e) (zip-node (zip-root z4))))

(test-group "zip-find"
  (define (zip-node= x)
    (lambda (y)
      (eq? x (zip-node y))))

  (define z0
    (list-zipper '(a (b 1) (c 2))))

  (test 'c (zip-node (zip-find z0 (zip-node= 'c))))

  (test #f (zip-find (zip-find z0 (zip-node= 'b)) (zip-node= 'c)))

  (test '(a (b X) (c 2))
        (and-let* ((x (zip-find z0 (zip-node= 'b)))
                   (x (zip-up x))
                   (x (zip-find x (zip-node= 1)))
                   (x (zip-replace x 'X))
                   (x (zip-root x)))
          (zip-node x))))

(test-group "zip-find-child"
  (define z0 (list-zipper '(1 (3 2 5) 4 7)))
  (define (zip-node-even? x)
    (let ((n (zip-node x)))
      (and (number? n) (even? n))))
  (test 2 (zip-node (zip-find z0 zip-node-even?)))
  (test 4 (zip-node (zip-find-child z0 zip-node-even?))))

(test-group "zip-filter"
  (define z (list-zipper '(1 foo bar 2 3 qux)))
  (test '(1 2 3) (map zip-node (zip-filter z (o number? zip-node)))))

(test-group "zip-cut, zip-scope, zip-unscope"
  (define z0 (list-zipper '(a b)))

  (test 'a (and-let* ((x (zip-down z0))
                      (x (zip-scope x))
                      (x (zip-root x)))
             (zip-node x)))

  (test (zip-node z0)
        (and-let* ((x (zip-down z0))
                   (x (zip-scope x))
                   (x (zip-unscope x))
                   (x (zip-root x)))
          (zip-node x)))

  (test-assert (and-let* ((x (zip-down z0))
                          (x (zip-scope x)))
                 (not (zip-next x))))

  (test 'a (and-let* ((x (zip-down z0))
                      (x (zip-cut x))
                      (x (zip-root x)))
             (zip-node x)))

  (test '(a (X c))
        (and-let* ((x (list-zipper '(a (b c))))
                   (x (zip-down x))
                   (x (zip-right x))
                   (x (zip-scope x))
                   (x (zip-down x))
                   (x (zip-replace x 'X))
                   (x (zip-root x))
                   (x (zip-unscope x))
                   (x (zip-root x)))
          (zip-node x)))

  (define z1 (list-zipper '(((3) 2) 1)))

  (test (zip-node z1)
        (and-let* ((x (zip-down z1))
                   (x (zip-scope x))
                   (x (zip-down x))
                   (x (zip-scope x))
                   (x (zip-down x))
                   (x (zip-scope x))
                   (x (zip-root x))
                   (x (zip-unscope x))
                   (x (zip-root x))
                   (x (zip-unscope x))
                   (x (zip-root x))
                   (x (zip-unscope x))
                   (x (zip-root x)))
          (zip-node x)))

  (test-error (and-let* ((x (zip-down z))
                         (x (zip-cut x)))
                (zip-unscope x))))

(test-end)

(test-exit)
