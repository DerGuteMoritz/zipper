(module zipper

(zipper
 zipper?
 list-zipper
 zip-root
 zip-cut
 zip-scope
 zip-unscope
 zip-node
 zip-branch?
 zip-children
 zip-next
 zip-right
 zip-rightmost
 zip-left
 zip-leftmost
 zip-up
 zip-down
 zip-edit
 zip-insert-left
 zip-insert-right
 zip-append-child
 zip-prepend-child
 zip-replace
 zip-walk
 zip-filter
 zip-find
 zip-find-child
 zip-find-parent)

"zipper-impl.scm"

)
