;; A generic functional zipper implementation based on Rich Hickey's
;; clojure.zip library with several extensions
;;
;; Copyright (c) 2013-2015 Moritz Heidkamp. All rights reserved.
;;
;; The use and distribution terms for this software are covered by the
;; Eclipse Public License 1.0
;; (http://opensource.org/licenses/eclipse-1.0.php) which can be found
;; in the file COPYING at the root of this distribution. By using this
;; software in any fashion, you are agreeing to be bound by the terms
;; of this license.
;;
;; You must not remove this notice, or any other, from this software.

(import chicken scheme)
(use srfi-1)

(define-record-type zipper
  (make-zipper impl node path unscoped)
  zipper?
  (impl zipper-impl)
  (node zip-node)
  (path zipper-path)
  (unscoped zipper-unscoped))

(define-record-type zipper-impl
  (make-zipper-impl name branch? children make-node)
  zipper-impl?
  (name zipper-impl-name)
  (branch? zipper-impl-branch?)
  (children zipper-impl-children)
  (make-node zipper-impl-make-node))

(define-record-type zipper-path
  (make-path left parent changed? right)
  path?
  (left path-left)
  (parent path-parent)
  (changed? path-changed?)
  (right path-right))

(define-record-printer (zipper x out)
  (display "#<" out)
  (display (zipper-impl-name (zipper-impl x)) out)
  (display " " out)
  (write (zip-node x) out)
  (display ">" out))

(define (copy-zipper zipper
                     #!key
                     (node (zip-node zipper))
                     (path (zipper-path zipper))
                     unscoped)
  (make-zipper (zipper-impl zipper)
               node
               path
               unscoped))

(define (copy-path path
                   #!key
                   (left (path-left path))
                   (parent (path-parent path))
                   (changed (path-changed? path))
                   (right (path-right path)))
  (make-path left
             parent
             changed
             right))

(define (zipper branch? children #!optional make-node (name 'zipper))
  (let ((impl (make-zipper-impl name branch? children make-node)))
    (lambda (root)
      (make-zipper impl root #f #f))))

(define list-zipper
  (zipper pair?
          values
          (lambda (node children)
            children)
          'list-zipper))

(define (zip-branch? zipper)
  ((zipper-impl-branch? (zipper-impl zipper)) (zip-node zipper)))

(define (make-node zipper node children)
  ((zipper-impl-make-node (zipper-impl zipper)) node children))

(define (zip-children zipper)
  (if (zip-branch? zipper)
      ((zipper-impl-children (zipper-impl zipper)) (zip-node zipper))
      (error 'children "Can't be called on a leaf node" zipper)))

(define (zip-down zipper)
  (and (zip-branch? zipper)
       (let ((cs (zip-children zipper)))
         (and (pair? cs)
              (copy-zipper zipper
                           node: (car cs)
                           path: (make-path '()
                                            zipper
                                            #f
                                            (cdr cs)))))))

(define (zip-up zipper)
  (and-let* ((path   (zipper-path zipper))
             (parent (path-parent path)))
    (if (path-changed? path)
        (copy-zipper parent
                     unscoped: (zipper-unscoped parent)
                     path: (let ((ppath (zipper-path parent)))
                             (if ppath
                                 (copy-path ppath changed: #t)
                                 (make-path #f #f #t #f)))
                     node: (make-node parent
                                      (zip-node parent)
                                      (append-reverse
                                       (path-left path)
                                       (cons (zip-node zipper)
                                             (path-right path)))))
        parent)))

(define ((horizontal-navigation nodes new-node new-path) zipper)
  (and-let* ((path (zipper-path zipper))
             (ns   (nodes path)))
    (and (pair? ns)
         (copy-zipper zipper
                      node: (new-node ns)
                      path: (new-path ns (zip-node zipper) path)))))

(define zip-left
  (horizontal-navigation
   path-left
   car
   (lambda (ls node path)
     (copy-path path
                left: (cdr ls)
                right: (cons node (path-right path))))))

(define zip-leftmost*
  (horizontal-navigation
   path-left
   last
   (lambda (ls node path)
     (copy-path path
                left: '()
                right: (cdr (append-reverse ls (cons node (path-right path))))))))

(define (zip-leftmost zipper)
  (or (zip-leftmost* zipper) zipper))

(define zip-right
  (horizontal-navigation
   path-right
   car
   (lambda (rs node path)
     (copy-path path
                left: (cons node (path-left path))
                right: (cdr rs)))))

(define zip-rightmost*
  (horizontal-navigation
   path-right
   last
   (lambda (rs node path)
     (copy-path path
                left: (cdr (append-reverse rs (cons node (path-left path))))
                right: '()))))

(define (zip-rightmost zipper)
  (or (zip-rightmost* zipper) zipper))

(define (zip-next zipper)
  (or (and (zip-branch? zipper) (zip-down zipper))
      (zip-right zipper)
      (let loop ((zipper zipper))
        (and-let* ((p (zip-up zipper)))
          (or (zip-right p) (loop p))))))

(define (zip-root zipper)
  (let ((p (zip-up zipper)))
    (if p (zip-root p) zipper)))

(define (ensure-editable! location zipper)
  (unless (zipper-impl-make-node (zipper-impl zipper))
    (error location "Zipper is not editable" zipper)))

(define (%zip-replace zipper node)
  (copy-zipper zipper
               node: node
               unscoped: (zipper-unscoped zipper)
               path: (and-let* ((path (zipper-path zipper)))
                       (copy-path path changed: #t))))

(define (zip-replace zipper node)
  (ensure-editable! 'zip-replace zipper)
  (%zip-replace zipper node))

(define (zip-edit zipper f . args)
  (ensure-editable! 'zip-edit zipper)
  (%zip-replace zipper (apply f (zip-node zipper) args)))

(define (update-node zipper f . args)
  (%zip-replace zipper (make-node zipper (zip-node zipper) (apply f args))))

(define (zip-prepend-child zipper child)
  (ensure-editable! 'zip-prepend-child zipper)
  (update-node zipper (lambda () (cons child (zip-children zipper)))))

(define (zip-append-child zipper child)
  (ensure-editable! 'zip-append-child zipper)
  (update-node zipper (lambda () (append (zip-children zipper) (list child)))))

(define ((zip-insert-sibling name field siblings) zipper sibling)
  (ensure-editable! name zipper)
  (let ((path (zipper-path zipper)))
    (if path
        (let ((new-siblings (cons sibling (siblings path))))
          (copy-zipper zipper path: (copy-path path field new-siblings changed: #t)))
        (error name "Sibling insertion at root is not possible" zipper sibling))))

(define zip-insert-left
  (zip-insert-sibling 'zip-insert-left left: path-left))

(define zip-insert-right
  (zip-insert-sibling 'zip-insert-right right: path-right))

(define (zip-cut zipper)
  (copy-zipper zipper
               unscoped: #f
               path: #f))

(define (zip-scope zipper)
  (copy-zipper zipper
               unscoped: zipper
               path: #f))

(define (zip-unscope zipper)
  (cond ((zipper-unscoped zipper) =>
         (lambda (unscoped)
           (let ((path (zipper-path zipper)))
             (if (and path (path-changed? path))
                 (copy-zipper zipper
                              path: (and-let* ((ppath (zipper-path unscoped)))
                                      (copy-path ppath changed: #t))
                              unscoped: (zipper-unscoped unscoped))
                 unscoped))))
        ((zipper-path zipper) =>
         (lambda (path)
           (let* ((new-parent (zip-unscope (path-parent path)))
                  (new-path (copy-path path parent: new-parent)))
             (copy-zipper zipper path: new-path))))
        (else
         (error "Unscoped zipper can't be unscoped" zipper))))

(define (zip-walk zipper proc init)
  (let loop ((acc init)
             (zipper (zip-scope zipper)))
    (if zipper
        (loop (proc zipper acc)
              (zip-next zipper))
        acc)))

(define (zip-filter zipper pred?)
  (reverse! (zip-walk zipper
                      (lambda (zipper result)
                        (if (pred? zipper)
                            (cons (zip-unscope zipper) result)
                            result))
                      '())))

(define (zip-find zipper pred?)
  (let loop ((zipper (zip-scope zipper)))
    (and zipper
         (if (pred? zipper)
             (zip-unscope zipper)
             (loop (zip-next zipper))))))

(define (zip-find-child zipper pred?)
  (let loop ((zipper (zip-down zipper)))
    (and zipper
         (if (pred? zipper)
             zipper
             (loop (zip-right zipper))))))

(define (zip-find-parent zipper pred?)
  (let loop ((zipper (zip-up zipper)))
    (and zipper
         (if (pred? zipper)
             zipper
             (loop (zip-up zipper))))))
